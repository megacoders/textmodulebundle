<?php
namespace Megacoders\TextModuleBundle\Controller;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Gedmo\Translatable\Query\TreeWalker\TranslationWalker;
use Megacoders\PageBundle\Controller\Module\BaseModuleController;
use Megacoders\TextModuleBundle\Entity\Text;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TextController extends BaseModuleController
{
    /**
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        return $this->render('index', [
            'text' => $this->getText()
        ]);
    }

    /**
     * @return null|Text
     */
    protected function getText() {
        $contentId = $this->getModuleParameter("contentId");

        if (!$contentId) {
            return null;
        }

        /** @var EntityRepository $repository */
        $repository = $this->getDoctrine()->getRepository(Text::class);
        $queryBuilder = $repository->createQueryBuilder('t', 't.id');

        $queryBuilder
            ->andWhere('t.id = :id')
            ->setParameter('id', $contentId);

        /** @var Text $text */
        $text = $queryBuilder->getQuery()
            ->setHint(Query::HINT_CUSTOM_OUTPUT_WALKER, TranslationWalker::class)
            ->getSingleResult();

        return $text;
    }
}
