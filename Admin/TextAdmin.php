<?php

namespace Megacoders\TextModuleBundle\Admin;


use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Megacoders\AdminBundle\Admin\BaseAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TextAdmin extends BaseAdmin
{

    /**
     * @var string
     */
    protected $baseRoutePattern = 'content/text';

    /**
     * @var array
     */
    protected $datagridValues = array(
        '_page' => 1,
        '_sort_order' => 'ASC',
        '_sort_by' => 'name',
    );

    protected function configureDatagridFilters(DatagridMapper $filterMapper)
    {
        $filterMapper->add('name', null, ['label' => 'admin.entities.text.name']);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('name', null, ['label' => 'admin.entities.text.name'])
            ->add('_action', null, [
                'label' => 'admin.actions._actions',
                'actions' => ['edit' => [], 'delete' => []]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->with('admin.labels.text')
                ->add('name', null, ['label' => 'admin.entities.text.name'])
                ->add('content', CKEditorType::class, ['label' => 'admin.entities.text.content'])
            ->end();
    }

}
