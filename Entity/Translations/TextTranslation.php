<?php

namespace Megacoders\TextModuleBundle\Entity\Translations;

use Doctrine\ORM\Mapping as ORM;
use Megacoders\TextModuleBundle\Entity\Text;
use Sonata\TranslationBundle\Model\Gedmo\AbstractPersonalTranslation;

/**
 * @ORM\Entity
 * @ORM\Table(
 *     name="text_translation",
 *     uniqueConstraints={@ORM\UniqueConstraint(name="lookup_unique_text_translation_idx", columns={
 *         "locale", "object_id", "field"
 *     })}
 * )
 */
class TextTranslation extends AbstractPersonalTranslation
{
    /**
     * @ORM\ManyToOne(targetEntity="Megacoders\TextModuleBundle\Entity\Text", inversedBy="translations")
     * @ORM\JoinColumn(name="object_id", referencedColumnName="id", onDelete="CASCADE")
     * @var Text
     */
    protected $object;

}
